<?php

/**
 * @file
 * Contains \Drupal\cointools_fiat\Form\FiatForm.
 */

namespace Drupal\cointools_fiat\Form;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\cointools_fiat\Fiat;
use Drupal\cointools_fiat\FiatCurrencies;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Defines a form to configure maintenance settings for this site.
*/
class FiatForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_fiat';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currencies = FiatCurrencies::currencies();
    $rates = Fiat::getRates();
    $rows = [];

    foreach ($currencies as $code => $info) {
      if (!isset($rates[$code])) {
        continue;
      }
      $fiat_amount = [
        '#theme' => 'cointools_fiat_amount',
        '#amount' => 1e8,
        '#code' => $code,
        '#explicit' => FALSE,
      ];
      $rows[] = [
        [
          'data' => $code,
          'class' => ['cointools-monospace'],
        ],
        $info['label'],
        drupal_render($fiat_amount),
        [
          'data' => number_format(1e8 / $rates[$code]),
          'style' => 'text-align: right;',
        ],
      ];
    }

    if (count($rows)) {
      $amount = [
        '#type' => 'cointools_amount',
      ];
      $header = [
        t("Code"),
        t("Name"),
        SafeMarkup::set(drupal_render($amount) . '&#8199;≈'),
        [
          'data' => t("Satoshis"),
          'class' => [RESPONSIVE_PRIORITY_LOW],
          'style' => 'text-align: right;',
        ],
      ];

      $form['rates'] = [
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
        '#attributes' => ['style' => 'width: auto;'],
      ];
    }
    $form['source'] = ['#markup' => Fiat::currentSource()];

    $updated_timestamp = \Drupal::state()->get('cointools_fiat.timestamp');
    if (isset($updated_timestamp)) {
      $updated_markup = t("Last updated !time ago.", ['!time' => \Drupal::service('date.formatter')->formatInterval(REQUEST_TIME - $updated_timestamp)]);
    }
    else {
      $updated_markup = t("Exchange rates never retrieved.");
    }
    $form['updated'] = ['#markup' => $updated_markup];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Update now",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      Fiat::updateRates();
    }
    catch (\Exception $e) {
      drupal_set_message(t("Failed to update exchange rates."), 'error');
      return;
    }
    drupal_set_message(t("Fiat exchange rates updated."));
  }

}
