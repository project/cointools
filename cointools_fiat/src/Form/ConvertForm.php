<?php
/**
* @file
* Contains \Drupal\cointools_fiat\Form\ConvertForm.
*/

namespace Drupal\cointools_fiat\Form;

use Drupal\Core\Form\FormBase;
use Drupal\cointools_fiat\Fiat;
use Drupal\Core\Form\FormStateInterface;

/**
* Defines a form to configure maintenance settings for this site.
*/
class ConvertForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_fiat_convert';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    if (count($values)) {

      if ($values['quantity'] != 0) {
        $units = array_keys(cointools_units());
        $exp = array_search($values['unit'], $units);
        $amount = $values['quantity'] * pow(1000, $exp) * 100;
        $form['amount'] = [
          '#type' => 'cointools_amount',
          '#amount' => $amount,
          '#fiat' => TRUE,
          '#code' => $values['code'],
        ];
      }
    }

    $units = cointools_units();

    foreach ($units as $key => &$value) {
      $value = $key;
    }

    $form['unit'] = [
      '#type' => 'select',
      '#title' => "Amount",
      '#options' => $units,
      '#default_value' => 'mBTC',
      '#required' => TRUE,
    ];

    $form['quantity'] = [
      '#type' => 'number',
      '#step' => 'any',
      '#min' => 0,
      '#required' => TRUE,
    ];

    $user = \Drupal::currentUser();

    $form['code'] = [
      '#type' => 'select',
      '#title' => t("Fiat currency"),
      '#options' => Fiat::currencyOptions(),
      '#default_value' => \Drupal::service('user.data')->get('cointools_fiat', $user->id(), 'currency'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Convert",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
