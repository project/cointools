<?php

/**
 * @file
 * Contains \Drupal\cointools_fiat\Plugin\Block\CoinToolsFiatCurrentRate.
 */

namespace Drupal\cointools_fiat\Plugin\Block;

use Drupal\cointools_fiat\Fiat;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Bitcoin current rate' block.
 *
 * @Block(
 *   id = "cointools_fiat_current_rate",
 *   admin_label = @Translation("Bitcoin current rate")
 * )
 */
class CoinToolsFiatCurrentRate extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_source' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['display_source'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Display source"),
      '#description' => $this->t("Show the exchange rate source."),
      '#default_value' => $this->configuration['display_source'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display_source'] = $form_state->getValue('display_source');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $build['rate'] = [
      '#type' => 'cointools_amount',
      '#fiat' => TRUE,
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    $build['source'] = [
      '#markup' => Fiat::currentSource(),
      '#access' => $this->configuration['display_source'],
    ];

    return $build;
  }

}
