/**
 * @file
 * Continually checks if a payment has been completed.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.behaviors.coinToolsDaemonCheckPayment = {
    attach: function (context) {
      var active = false;
      window.setInterval(function() {
        if (active) {
          return;
        }
        active = true;
        $.ajax({
          url: Drupal.url("bitcoin-payment/" + drupalSettings.cointools.pid + "/check"),
          dataType: "html"
        })
        .done(function (data, textStatus, jqXHR) {
          // If a form id is supplied, submit the form.
          if (drupalSettings.cointools.form_id) {
            $("#" + drupalSettings.cointools.form_id).submit();
          }
          // Otherwise just reload the page.
          else {
            window.location.reload(false);
          }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          active = false;
        });
      }, 250);
    }
  };

})(jQuery, Drupal, drupalSettings);
