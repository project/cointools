<?php

namespace Drupal\cointools_daemon;

use GuzzleHttp\Url as GuzzleUrl;
use GuzzleHttp\Exception\RequestException;
use Graze\GuzzleHttp\JsonRpc\Client as JsonRpcClient;
use Drupal\cointools\CoinTools;
use Drupal\Core\Url;

class Client {

  private $client;
  private $fees = [];

  public static function factory($report_error = TRUE) {
    $config = \Drupal::config('cointools_daemon.settings');
    $uri = new GuzzleUrl($config->get('scheme'), $config->get('hostname'), $config->get('username'), $config->get('password'), $config->get('port'));
    $client = new Client(JsonRpcClient::factory($uri));

    try {
      $client->request('getinfo');
    }
    catch (RequestException $exception) {
      $message = t("Unable to connect to bitcoind. Check <a href=\"@url\">configuration</a>.", ['@url' => Url::fromRoute('cointools_daemon.settings')->toString()]);
      if ($report_error) {
        drupal_set_message($message, 'error');
      }
      throw new \Exception($message);
    }

    return $client;
  }

  public function __construct(JsonRpcClient $client) {
    $this->client = $client;
  }

  public function request($method, array $params = []) {
    return $this->client->send($this->client->request(1, $method, $params))->getRpcResult();
  }

  public function balanceItem() {
    $confirmed = CoinTools::bitcoinToSatoshi($this->request('getbalance'));
    $unconfirmed = CoinTools::bitcoinToSatoshi($this->request('getbalance', ['', 0]));

    $items['confirmed'] = [
      '#type' => 'item',
      '#title' => "Confirmed balance",
      'amount' => [
        '#type' => 'cointools_amount',
        '#amount' => $confirmed,
        '#fiat' => TRUE,
      ],
    ];

    if ($unconfirmed > $confirmed) {
      $items['unconfirmed'] = [
        '#type' => 'item',
        '#title' => "Unconfirmed balance",
        'amount' => [
          '#type' => 'cointools_amount',
          '#amount' => $unconfirmed,
          '#fiat' => TRUE,
        ],
      ];
    }

    return $items;
  }

  public function timeSinceLastBlock() {
    $hash = $this->request('getbestblockhash');
    $block = $this->request('getblock', [$hash]);
    return REQUEST_TIME - $block['time'];
  }

  public function averageTimeUntilNextBlock() {
    $difficulty = $this->request('getdifficulty');
    $hashrate = $this->request('getnetworkhashps');
    return $difficulty * pow(2, 32) / $hashrate;
  }

  /**
   * Determines the fee necessary to get a 1kB transaction in a block within
   * nblocks.
   *
   * @param $nblocks
   *   Number of blocks it should take for the transaction to confirm.
   *
   * @return int
   *   Fee size in satoshis.
   */
  public function estimateFee($nblocks = 1) {
    if (!isset($this->fees[$nblocks])) {
      $this->fees[$nblocks] = $this->request('estimatefee', [$nblocks]);
    }
    $fee = $this->fees[$nblocks];
    if ($fee == -1) {
      throw new \Exception();
    }
    else {
      return CoinTools::bitcoinToSatoshi($fee);
    }
  }

  /**
   * Determines the fee necessary to get a transaction in a block within
   * nblocks.
   *
   * @param $inputs
   *   Number of inputs.
   * @param $outputs
   *   Number of outputs.
   * @param $nblocks
   *   Number of blocks it should take for the transaction to confirm.
   *
   * @return int
   *   Fee size in satoshis.
   */
  public function transactionEstimateFee($inputs, $outputs = 1, $nblocks = 1) {
    // @see https://bitcoin.stackexchange.com/questions/1195/how-to-calculate-transaction-size-before-sending
    $size = 149 * $inputs + 34 * $outputs + 10;
    $fee_per_kb = $this->estimateFee($nblocks);
    return ceil($size * $fee_per_kb / 1000);
  }

  public function transactionLoad($txid) {
    $raw = $this->request('getrawtransaction', [$txid]);
    return $this->request('decoderawtransaction', [$raw]);
  }

  public function transactionSendNew(array $inputs, array $outputs) {
    $raw = $this->request('createrawtransaction', [$inputs, $outputs]);
    $raw = $this->request('signrawtransaction', [$raw]);
    return $this->request('sendrawtransaction', [$raw['hex']]);
  }

  /**
   * Forwards a bitcoin transaction to one or more addresses with defined
   * proportionality.
   *
   * @param $txid
   *   txid of transaction to forward.
   * @param array $addresses
   *   List of addresses that can be spent from.
   * @param array $outputs
   *   Where to forward the bitcoin to.
   *   Keys are destinations addresses.
   *   Values are proportional quantities.
   * @param bool $tx_confirm_target
   *   The fee should be calculated for the transaction to reach the blockchain
   *   after this many blocks. default = 1
   *
   * @return string
   *   txid of the forwarding transaction.
   */
  public function forwardTransaction($txid, array $addresses, array $outputs, $tx_confirm_target = 1) {
    $transaction_in = $this->transactionLoad($txid);
    // Find inputs and amount for new transaction.
    $inputs = [];
    $transaction_amount = 0;
    foreach ($transaction_in['vout'] as $vout) {
      // Is this output for one of our addresses?
      if (in_array($vout['scriptPubKey']['addresses'][0], $addresses)) {
        // Has this output been spent yet?
        try {
          $this->request('gettxout', [$txid, $vout['n']]);
          $inputs[] = [
            'txid' => $txid,
            'vout' => $vout['n'],
          ];
          $transaction_amount += CoinTools::bitcoinToSatoshi($vout['value']);
        }
        catch (\Exception $e) {}
      }
    }
    // Remove the miner fee from the transaction amount.
    $transaction_amount -= $this->transactionEstimateFee(count($inputs), count($outputs));
    // Divide up the pie according to the correct proportions.
    $ratio = $transaction_amount / array_sum($outputs);
    foreach ($outputs as $address => &$amount) {
      $amount *= $ratio;
      // Eliminate dust outputs.
      if ($amount < 546) {
        unset($outputs[$address]);
        continue;
      }
      $amount = CoinTools::satoshiToBitcoin($amount);
    }
    // Make sure there is something to send.
    if (empty($outputs)) {
      throw new \Exception("No bitcoin to send.");
    }
    // Send the transaction.
    return $this->transactionSendNew($inputs, $outputs);
  }

  public function send($address, $amount) {
    if ($amount < 546) {
      throw new \Exception();
    }
    $inputs = [];
    // Get the UTXOs we can use.
    $utxos = $this->request('listunspent', [0]);
    // Convert them to satoshis.
    foreach ($utxos as &$utxo) {
      $utxo['amount'] = CoinTools::bitcoinToSatoshi($utxo['amount']);
    }
    // Sort them starting with the biggest.
    usort($utxos, '\Drupal\cointools_daemon\Client::utxoCompare');
    // Determine fee for simplest transaction.
    $fee = $this->transactionEstimateFee(1, 2);
    // Find the smallest single UTXO that is big enough.
    foreach ($utxos as $utxo) {
      if ($utxo['amount'] >= $amount + $fee) {
        $inputs = [[
          'txid' => $utxo['txid'],
          'vout' => $utxo['vout'],
        ]];
        $input_amount = $utxo['amount'];
      }
      else {
        break;
      }
    }
    if (!count($inputs)) {
      $input_amount = 0;
      // Try combining UTXOs to make a big enough total.
      foreach ($utxos as $utxo) {
        $inputs[] = [
          'txid' => $utxo['txid'],
          'vout' => $utxo['vout'],
        ];
        $input_amount += $utxo['amount'];
        $fee = $this->transactionEstimateFee(count($inputs), 2);
        // Do we have enough yet?
        if ($input_amount >= $amount + $fee) {
          break;
        }
      }
    }
    // Calculate change.
    $change = $input_amount - ($amount + $fee);
    // Are there enough funds?
    if ($change < 0) {
      throw new \Exception();
    }
    $outputs = [$address => CoinTools::satoshiToBitcoin($amount)];
    // Is there any change?
    if ($change >= 546) {
      $change_address = $this->request('getrawchangeaddress');
      $outputs[$change_address] = CoinTools::satoshiToBitcoin($change);
    }
    // Send the transaction.
    return $this->transactionSendNew($inputs, $outputs);
  }

  static function utxoCompare($a, $b) {
    return $a['amount'] < $b['amount'];
  }

}
