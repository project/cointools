<?php

/**
 * @file
 * Contains \Drupal\cointools_daemon\Plugin\field\formatter\AddressLinkFormatter.
 */

namespace Drupal\cointools_daemon\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Plugin implementation of the 'cointools_address_text' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_address_link",
 *   label = @Translation("Link"),
 *   field_types = {
 *     "cointools_address",
 *   },
 * )
 */
class AddressLinkFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $address = BitcoinLib::base58_encode(bin2hex($item->value));
      $elements[$delta] = [
        '#theme' => 'cointools_daemon_address_link',
        '#address' => $address,
      ];
    }

    return $elements;
  }

}
