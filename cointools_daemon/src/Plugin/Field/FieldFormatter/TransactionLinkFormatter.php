<?php

/**
 * @file
 * Contains \Drupal\cointools_daemon\Plugin\field\formatter\TransactionLinkFormatter.
 */

namespace Drupal\cointools_daemon\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'cointools_transaction_text' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_transaction_link",
 *   label = @Translation("Link"),
 *   field_types = {
 *     "cointools_transaction",
 *   },
 * )
 */
class TransactionLinkFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'cointools_transaction_link',
        '#txn_id' => bin2hex($item->value),
      ];
    }

    return $elements;
  }

}
