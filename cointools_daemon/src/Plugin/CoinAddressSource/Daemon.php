<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\CoinAddressSource\Daemon.
 */

namespace Drupal\cointools_daemon\Plugin\CoinAddressSource;

use Drupal\cointools\CoinTools;
use Drupal\cointools\CoinAddressSourceBase;
use Drupal\cointools_daemon\Client as DaemonClient;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides addresses from a coin daemon.
 *
 * @Plugin(
 *   id = "cointools_daemon",
 *   title = @Translation("Daemon hot-wallet"),
 * )
 */
class Daemon extends CoinAddressSourceBase {

  function getAddress() {
    $client = DaemonClient::factory();
    return $client->request('getnewaddress');
  }

  function form() {
    $form['forwarding_address'] = [
      '#title' => t("Forwarding address"),
      '#type' => 'textfield',
      '#attributes' => ['class' => ['cointools-monospace']],
      '#size' => 34,
      '#description' => t("Received payments will automatically be forwarded to this address after one confirmation."),
    ];

    return $form;
  }

  function validate(FormStateInterface $form_state, array $values) {
    if ($values['forwarding_address'] != '') {
      if (!CoinTools::validateAddress($values['forwarding_address'])) {
        $message = CoinTools::testnet() ? t("Bitcoin testnet address is invalid.") : t("Bitcoin address is invalid.");
        $form_state->setErrorByName('cointools_address_source_config_cointools_daemon', $message);
      }
    }
  }

  function confirmed($txid, $address, array $config) {
    if ($config['forwarding_address'] != '') {
      DaemonClient::factory()->forwardTransaction($txid, [$address], [$config['forwarding_address'] => 1]);
    }
  }

}
