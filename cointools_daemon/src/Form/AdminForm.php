<?php
/**
* @file
* Contains \Drupal\cointools_daemon\Form\AdminForm.
*/

namespace Drupal\cointools_daemon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Url;
use Graze\GuzzleHttp\JsonRpc\Client as JsonRpcClient;
use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;

/**
* Defines a form to configure maintenance settings for this site.
*/
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_daemon';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cointools_daemon.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cointools_daemon.settings');

    $form['scheme'] = [
      '#type' => 'select',
      '#title' => t("Scheme"),
      '#options' => [
        'http' => 'HTTP',
        'https' => 'HTTPS',
      ],
      '#default_value' => $config->get('scheme'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => t("Username"),
      '#default_value' => $config->get('username'),
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => t("Password"),
    ];
    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => t("Hostname"),
      '#default_value' => $config->get('hostname'),
    ];
    $form['port'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 65535,
      '#step' => 1,
      '#title' => t("Port"),
      '#default_value' => $config->get('port'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $uri = new Url($values['scheme'], $values['hostname'], $values['username'], $values['password'], $values['port']);

    try {
      $client = new DaemonClient(JsonRpcClient::factory($uri));
      $info = $client->request('getinfo');
    }
    catch (\Exception $exception) {
      $form_state->setErrorByName('', t("Unable to connect."));
      return;
    }

    if ($info['testnet'] != CoinTools::testnet()) {
      $message = t("That bitcoind is running @network; this does not match Coin Tools <a href=\"@url\">configuration</a>.", [
        '@network' => $info['testnet'] ? 'testnet' : 'mainnet',
        '@url' => \Drupal::url('cointools.settings'),
      ]);
      $form_state->setErrorByName('', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('cointools_daemon.settings');
    $settings = ['scheme', 'username', 'password', 'hostname', 'port'];
    $values = $form_state->getValues();
    foreach ($settings as $setting) {
      $config->set($setting, $values[$setting]);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
