<?php
/**
* @file
* Contains \Drupal\cointools_daemon\Form\SendForm.
*/

namespace Drupal\cointools_daemon\Form;

use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Graze\Guzzle\JsonRpc\JsonRpcClient;
use Drupal\Core\Url;

/**
* Defines a form to configure maintenance settings for this site.
*/
class SendForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_daemon_send';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = &$form_state->getStorage();
    $values = isset($storage['values']) ? $storage['values'] : [];
    $form['actions']['#type'] = 'actions';

    if (!isset($storage['confirm'])) {
      try {
        $client = DaemonClient::factory();
      }
      catch (\Exception $exception) {
        return [];
      }
      $form['balance'] = $client->balanceItem();
      $units = cointools_units();
      foreach ($units as $key => &$value) {
        $value = $key;
      }

      $form['unit'] = [
        '#type' => 'select',
        '#title' => "Amount to send",
        '#options' => $units,
        '#default_value' => isset($values['unit']) ? $values['unit'] : 'mBTC',
        '#required' => TRUE,
      ];

      $form['quantity'] = [
        '#type' => 'number',
        '#step' => 'any',
        '#min' => 0,
        '#default_value' => isset($values['quantity']) ? $values['quantity'] : NULL,
        '#required' => TRUE,
      ];

      $form['address'] = [
        '#type' => 'textfield',
        '#title' => "Destination address",
        '#default_value' => isset($values['address']) ? $values['address'] : NULL,
        '#required' => TRUE,
        '#size' => 34,
      ];

      $form['actions']['send'] = [
        '#type' => 'submit',
        '#value' => "Send",
        '#validate' => [[get_class($this), 'validateFormSend']],
        '#submit' => [[get_class($this), 'submitFormSend']],
      ];
    }
    else {
      $form['amount'] = [
        '#type' => 'item',
        '#title' => "Amount",
        'amount' => [
          '#type' => 'cointools_amount',
          '#amount' => $storage['amount'],
          '#fiat' => TRUE,
        ],
      ];

      $form['address'] = [
        '#type' => 'item',
        '#title' => "Address",
        '#markup' => $values['address'],
      ];

      $form['actions']['yes'] = [
        '#type' => 'submit',
        '#value' => "Confirm",
        '#submit' => [[get_class($this), 'submitFormConfirm']],
      ];

      $form['actions']['no'] = [
        '#type' => 'submit',
        '#value' => "Cancel",
        '#submit' => [[get_class($this), 'submitFormCancel']],
      ];
    }

    return $form;
  }

  /**
   * Form validation handler for Send button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  static public function validateFormSend(array &$form, FormStateInterface $form_state) {
    if (!CoinTools::validateAddress($form_state->getValue('address'))) {
      $message = CoinTools::testnet() ? t("Bitcoin testnet address is invalid.") : t("Bitcoin address is invalid.");
      $form_state->setErrorByName('address', $message);
    }
  }

  /**
   * Form submission handler for Send button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  static public function submitFormSend(array &$form, FormStateInterface $form_state) {
    $storage = &$form_state->getStorage();
    $storage['values'] = $form_state->getValues();
    $units = array_keys(cointools_units());
    $exp = array_search($storage['values']['unit'], $units);
    $storage['amount'] = $storage['values']['quantity'] * pow(1000, $exp) * 100;
    $storage['confirm'] = TRUE;
    $form_state->setRebuild();
  }

  /**
   * Form submission handler for Confirm button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  static public function submitFormConfirm(array &$form, FormStateInterface $form_state) {
    $storage = &$form_state->getStorage();

    $amount_render = [
      '#type' => 'cointools_amount',
      '#amount' => $storage['amount'],
      '#fiat' => TRUE,
    ];
    $amount = drupal_render($amount_render);

    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $exception) {
      return [];
    }

    try {
      $txn_id = $client->send($storage['values']['address'], $storage['amount']);
    }
    catch (\Exception $e) {
      unset($storage['confirm']);
      $form_state->setRebuild();

      $message_render = [
        '#theme' => 'cointools_daemon_send_failure',
        '#amount' => $amount,
        '#address' => $storage['values']['address'],
      ];

      $message = drupal_render($message_render);
      drupal_set_message($message, 'error');
      return;
    }

    $message_render = [
      '#theme' => 'cointools_daemon_send_success',
      '#amount' => $amount,
      '#address' => $storage['values']['address'],
    ];

    $message = drupal_render($message_render);
    $url = Url::fromRoute('cointools_daemon.transaction', ['txn_id' => $txn_id], ['html' => TRUE]);
    drupal_set_message(\Drupal::l($message, $url));
  }

  /**
   * Form submission handler for Cancel button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  static public function submitFormCancel(array &$form, FormStateInterface $form_state) {
    $storage = &$form_state->getStorage();
    unset($storage['confirm']);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
