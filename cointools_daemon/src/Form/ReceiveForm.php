<?php
/**
* @file
* Contains \Drupal\cointools_daemon\Form\AdminForm.
*/

namespace Drupal\cointools_daemon\Form;

use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Graze\Guzzle\JsonRpc\JsonRpcClient;

/**
* Defines a form to configure maintenance settings for this site.
*/
class ReceiveForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_daemon';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $exception) {
      return [];
    }

    $values = $form_state->getValues();
    if (count($values)) {
      $address = $client->request('getnewaddress');

      $form['address'] = [
        '#markup' => '<span class="cointools-monospace">' . $address . '</span><br />',
      ];

      if ($values['quantity'] != 0) {
        $units = array_keys(cointools_units());
        $exp = array_search($values['unit'], $units);
        $amount = $values['quantity'] * pow(1000, $exp) * 100;
        $form['amount'] = [
          '#type' => 'cointools_amount',
          '#amount' => $amount,
          '#fiat' => TRUE,
          '#suffix' => '</br />',
        ];
      }

      $uri_components = [
        'address' => $address,
        'amount' => isset($amount) ? $amount : NULL,
        'label' => $values['label'],
      ];
      $form['qr'] = [
        '#type' => 'cointools_qr',
        '#uri' => CoinTools::bitcoinUri($uri_components),
        '#suffix' => '<hr /><br />',
      ];
    }

    $form['balance'] = $client->balanceItem();

    $units = cointools_units();

    foreach ($units as $key => &$value) {
      $value = $key;
    }

    $form['unit'] = [
      '#type' => 'select',
      '#title' => "Amount to receive",
      '#options' => $units,
      '#default_value' => 'mBTC',
    ];

    $form['quantity'] = [
      '#type' => 'number',
      '#step' => 'any',
      '#min' => 0,
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => "Label",
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => "Receive",
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
