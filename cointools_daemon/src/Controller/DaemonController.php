<?php

/**
 * @file
 * Contains \Drupal\cointools_daemon\Controller\DaemonController.
 */

namespace Drupal\cointools_daemon\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;

/**
 * Controller routines for Coin Daemon routes.
 */
class DaemonController extends ControllerBase {

  /**
   * Displays the site status report.
   *
   * @return string
   *   The current status of bitcoind.
   */
  public function status() {
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $exception) {
      return [];
    }

    $info = $client->request('getinfo');

    if (isset($info['version'])) {
      $rows[] = [t("Version"), $info['version']];
    }
    if (isset($info['protocolversion'])) {
      $rows[] = [t("Protocol version"), $info['protocolversion']];
    }
    if (isset($info['walletversion'])) {
      $rows[] = [t("Wallet version"), $info['walletversion']];
    }
    if (isset($info['balance'])) {
      $amount = [
        '#type' => 'cointools_amount',
        '#amount' => CoinTools::bitcoinToSatoshi($info['balance']),
        '#fiat' => TRUE,
      ];
      $rows[] = [t("Wallet balance"), drupal_render($amount)];
    }
    try {
      $fee = [
        '#type' => 'cointools_amount',
        '#amount' => $client->estimateFee(),
        '#fiat' => TRUE,
      ];
    }
    catch (\Exception $e) {
      $fee = [
        '#markup' => t("n/a"),
      ];
    }
    $rows[] = [t("Estimated fee / kB to get in next block"), drupal_render($fee)];
    if (isset($info['blocks'])) {
      $rows[] = [t("Blocks"), $info['blocks']];
    }
    $time = $client->timeSinceLastBlock();
    $rows[] = [t("Time since last block"), \Drupal::service('date.formatter')->formatInterval($time, 2)];
    $time = $client->averageTimeUntilNextBlock();
    $rows[] = [t("Average time until next block"), \Drupal::service('date.formatter')->formatInterval($time, 2)];
    if (isset($info['timeoffset'])) {
      $rows[] = [t("Time offset"), $info['timeoffset']];
    }
    if (isset($info['connections'])) {
      $rows[] = [t("Connections"), $info['connections']];
    }
    if (isset($info['proxy'])) {
      $rows[] = [t("Proxy"), ($info['proxy'] == '') ? t("No") : $info['proxy']];
    }
    if (isset($info['difficulty'])) {
      $rows[] = [t("Difficulty"), $info['difficulty']];
    }
    $rows[] = [t("Network hash rate"), round($client->request('getnetworkhashps') / 1e15) . ' PH/s'];
    if (isset($info['testnet'])) {
      $rows[] = [t("Testnet"), ($info['testnet'] == '') ? t("No") : t("Yes")];
    }
    if (isset($info['keypoololdest'])) {
      $rows[] = [t("Key pool oldest"), format_date($info['keypoololdest'])];
    }
    if (isset($info['keypoolsize'])) {
      $rows[] = [t("Key pool size"), $info['keypoolsize']];
    }
    if (isset($info['paytxfee'])) {
      $amount = [
        '#type' => 'cointools_amount',
        '#amount' => CoinTools::bitcoinToSatoshi($info['paytxfee']),
      ];
      $rows[] = [t("Pay transaction fee"), drupal_render($amount)];
    }
    if (isset($info['relayfee'])) {
      $amount = [
        '#type' => 'cointools_amount',
        '#amount' => CoinTools::bitcoinToSatoshi($info['relayfee']),
      ];
      $rows[] = [t("Minimum relay fee"), drupal_render($amount)];
    }
    if (isset($info['errors'])) {
      $rows[] = [t("Errors"), $info['errors']];
    }

    return [
      '#theme' => 'table',
      '#rows' => $rows,
    ];
  }

  /**
   * Displays the site status report.
   *
   * @return string
   *   The current status of bitcoind.
   */
  public function fees() {
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $exception) {
      return [];
    }
    $rows = [];
    $nblocks = 0;

    while(($fee = $client->request('estimatefee', [++$nblocks])) != -1) {
      $row = [];
      $row[] = $nblocks;
      $row[] = [
        'data' => \Drupal::service('date.formatter')->formatInterval($nblocks * $client->averageTimeUntilNextBlock(), 2),
        'style' => 'text-align: right;',
      ];
      $amount = [
        '#type' => 'cointools_amount',
        '#amount' => CoinTools::bitcoinToSatoshi($fee),
        '#fiat' => TRUE,
        '#fiat_decimal_places' => 4,
        '#explicit' => FALSE,
        '#unit' => 'µBTC',
        '#unit_display' => FALSE,
        '#decimal_places' => 2,
      ];
      $row[] = [
        'data' => drupal_render($amount),
        'style' => 'text-align: right;',
      ];
      $amount = [
        '#type' => 'cointools_amount',
        // 1 input, 2 outputs
        '#amount' => CoinTools::bitcoinToSatoshi($fee * 0.227),
        '#fiat' => TRUE,
        '#fiat_decimal_places' => 4,
        '#explicit' => FALSE,
        '#unit' => 'µBTC',
        '#unit_display' => FALSE,
        '#decimal_places' => 2,
      ];
      $row[] = [
        'data' => drupal_render($amount),
        'style' => 'text-align: right;',
      ];

      $rows[] = $row;
    }

    $header = [
      t("Blocks"),
      [
        'data' => t("Average time"),
        'style' => 'text-align: right;',
      ],
      [
        'data' => t("Fee per kB (bits)"),
        'style' => 'text-align: right;',
      ],
      [
        'data' => t("Smallest fee (bits)"),
        'style' => 'text-align: right;',
      ],
    ];
    return [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];
  }

  /**
   * Redirects to the page on getaddr.bitnodes.io for the node.
   */
  public function bitnodes() {
    $networkinfo = DaemonClient::factory()->request('getnetworkinfo');

    if (count($networkinfo['localaddresses'])) {
      $info = $networkinfo['localaddresses'][0];
      $url = 'https://getaddr.bitnodes.io/nodes/' . $info['address']. '-' . $info['port'] . '/';
      return new RedirectResponse($url);
    }
    else {
      drupal_set_message($this->t("Unable to determine node address and port."), 'error');
      return $this->redirect('cointools_daemon.status');
    }
  }

  /**
   * Displays the bitcoin transactions.
   *
   * @return string
   *   The transactions.
   */
  public function transactions() {
    $num_per_page = 100;
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $exception) {
      return [];
    }
    $count = count($client->request('listtransactions', ['', 2147483647]));
    $page = pager_default_initialize($count, $num_per_page);
    $transactions = $client->request('listtransactions', ['', $num_per_page, $page * $num_per_page]);
    $rows = [];

    foreach (array_reverse($transactions) as $transaction) {
      $transaction += ['address' => ''];
      $amount = [
        '#type' => 'cointools_amount',
        '#amount' => abs(CoinTools::bitcoinToSatoshi($transaction['amount'])),
        '#fiat' => FALSE,
        '#unit' => 'mBTC',
        '#unit_display' => FALSE,
        '#decimal_places' => 2,
      ];
      $fee = [
        '#type' => 'cointools_amount',
        '#amount' => abs(CoinTools::bitcoinToSatoshi(isset($transaction['fee']) ? $transaction['fee'] : 0)),
        '#fiat' => FALSE,
        '#unit' => 'mBTC',
        '#unit_display' => FALSE,
        '#decimal_places' => 2,
      ];
      $txn_url = Url::fromRoute('cointools_daemon.transaction', ['txn_id' => $transaction['txid']]);
      $address_render = [
        '#theme' => 'cointools_daemon_address_link',
        '#address' => $transaction['address'],
      ];
      if ($transaction['confirmations'] < 0) {
        $c = 'x';
      }
      elseif ($transaction['confirmations'] > 0) {
        $c = 'C';
      }
      else {
        $c = '';
      }
      $rows[] = [
        'data' => [
          $c,
          \Drupal::l(format_date($transaction['time'], 'short'), $txn_url),
          $transaction['category'],
          drupal_render($address_render),
          [
            'data' => isset($transaction['fee']) ? drupal_render($fee) : '',
            'style' => 'text-align: right;',
          ],
          [
            'data' => drupal_render($amount),
            'style' => 'text-align: right;',
          ],
        ],
        'style' => 'background-color: #' . (($transaction['category'] == 'receive') ? 'e9fde9' : 'fff1f2'),
      ];
    }

    $units = cointools_units();

    $header = [
      '',
      t("Time"),
      t("Type"),
      [
        'data' => t("Receiving address"),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      [
        'data' =>  SafeMarkup::set(t("Fee") . ' (' . $units['mBTC'] . ')'),
        'style' => 'text-align: right;',
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ],
      [
        'data' => SafeMarkup::set(t("Amount") . ' (' . $units['mBTC'] . ')'),
        'style' => 'text-align: right;',
      ],
    ];

    $output = [];
    $output['balance'] = $client->balanceItem();
    $output['table'] = ['#theme' => 'table', '#rows' => $rows, '#header' => $header];
    $output['pager'] = ['#type' => 'pager'];

    return $output;
  }

  /**
   * Displays transaction information.
   *
   * @return string
   *   The info.
   */
  public function transaction($txn_id) {
    // Check we are connected early so error message can appear.
    // TODO: Implement connection caching.
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $e) {
      return [];
    }
    return [
      '#type' => 'cointools_transaction',
      '#txn_id' => $txn_id,
    ];
  }

  /**
   * Redirects to the page on BlockTrail for the transaction.
   */
  public function transactionBlockTrail($txn_id) {
    $url = 'https://www.blocktrail.com/BTC/tx/' . $txn_id;
    return new RedirectResponse($url);
  }

  /**
   * Displays address information.
   *
   * @return string
   *   The info.
   */
  public function address($address) {
    // Check we are connected early so error message can appear.
    // TODO: Implement connection caching.
    try {
      $client = DaemonClient::factory();
    }
    catch (\Exception $e) {
      return [];
    }

    $addresses = $client->request('listreceivedbyaddress', [0]);
    $rows = [];

    foreach ($addresses as $info) {
      if ($info['address'] == $address) {
        $rows[] = [t("Address"), $info['address']];

        $amount_render = [
          '#theme' => 'cointools_amount',
          '#amount' => CoinTools::bitcoinToSatoshi($info['amount']),
          '#fiat' => TRUE,
        ];

        $rows[] = [t("Amount"), drupal_render($amount_render)];

        foreach ($info['txids'] as $id => $txn_id) {
          $transaction_render = [
            '#theme' => 'cointools_transaction_link',
            '#txn_id' => $txn_id,
          ];
          $rows[] = [t("Transaction") . ' #' . $id, drupal_render($transaction_render)];
        }
        break;
      }
    }

    return [
      '#type' => 'table',
      '#rows' => $rows,
    ];
  }

  /**
   * Redirects to the page on BlockTrail for the address.
   */
  public function addressBlockTrail($address) {
    $url = 'https://www.blocktrail.com/BTC/address/' . $address;
    return new RedirectResponse($url);
  }

}
