<?php

/**
 * Implements hook_drush_command().
 */
function cointools_daemon_drush_command() {
  $items = [];

  $items['bitcoind-walletnotify'] = [
    'description' => "Respond when a wallet transaction changes.",
    'arguments' => [
      'txid' => 'Transaction ID',
    ],
  ];

  return $items;
}
