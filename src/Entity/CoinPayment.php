<?php

/**
 * @file
 * Contains \Drupal\cointools\Entity\CoinPayment.
 */

namespace Drupal\cointools\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;
use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Defines the coin_payment entity class.
 *
 * @ContentEntityType(
 *   id = "coin_payment",
 *   label = @Translation("Coin Payment"),
 *   bundle_label = @Translation("Payment type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\cointools\CoinPaymentViewBuilder",
 *     "access" = "Drupal\cointools\CoinPaymentAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\cointools\Form\PaymentForm",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "coin_payment",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "pid",
 *     "bundle" = "type",
 *     "uuid" = "uuid"
 *   },
 *   bundle_entity_type = "coin_payment_type",
 *   field_ui_base_route = "entity.coin_payment_type.edit_form",
 *   permission_granularity = "bundle",
 *   links = {
 *     "canonical" = "cointools.view",
 *     "delete-form" = "cointools.delete_confirm",
 *     "edit-form" = "cointools.page_edit",
 *   }
 * )
 */
class CoinPayment extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // Don't do anything if this is not a new payment.
    if ($this->id()) {
      return;
    }
    try {
      if (\Drupal::moduleHandler()->moduleExists('cointools_daemon')) {
        $client = DaemonClient::factory();
      }
      else {
        throw new \Exception("No address source.");
      }
    }
    catch (\Exception $exception) {
      return;
    }
    $payment_type = entity_load('coin_payment_type', $this->bundle());
    $source = \Drupal::service('cointools.address_source_manager')->createInstance($payment_type->cointools_address_source);
    $address = $source->getAddress(unserialize($payment_type->cointools_address_source_config));
    if ($payment_type->cointools_address_source != 'cointools_daemon') {
      $client->request('importaddress', [$address, '', FALSE]);
    }
    $this->set('address', hex2bin(BitcoinLib::base58_decode($address)));
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['pid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Payment ID'))
      ->setDescription(t('The payment ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The payment UUID.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The payment type.'))
      ->setSetting('target_type', 'coin_payment_type')
      ->setReadOnly(TRUE)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ]);

    $fields['time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Time created'))
      ->setDescription(t('The time the payment was created.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE)
      ->setDefaultValue([0 => [
        'default_date_type' => 'now',
        'default_date' => 'now',
      ]])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('cointools_amount')
      ->setLabel(t('Amount'))
      ->setDescription(t('The amount to be paid.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'cointools_amount',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'cointools_amount',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['memo'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Memo'))
      ->setDescription(t('The payment memo.'))
      ->setReadOnly(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['expires'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Time expires'))
      ->setDescription(t('The time the payment expires.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('cointools_address')
      ->setLabel(t('Address'))
      ->setDescription(t('The Bitcoin address to receive the payment.'))
      ->setDisplayOptions('view', [
        'type' => 'cointools_address_link',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['paid'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Time paid'))
      ->setDescription(t('The time the payment was paid.'))
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['transactions'] = BaseFieldDefinition::create('cointools_transaction')
      ->setLabel(t('Supplied transactions'))
      ->setDescription(t('Transactions supplied by the customer.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'cointools_transaction_link',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['refund_to'] = BaseFieldDefinition::create('cointools_address')
      ->setLabel(t('Supplied refund address'))
      ->setDescription(t('Refund address supplied by the customer.'))
      ->setDisplayOptions('view', [
        'type' => 'cointools_address_link',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['customer_memo'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Supplied memo'))
      ->setDescription(t('Memo supplied by the customer.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 8,
      ]);

    return $fields;
  }

}
