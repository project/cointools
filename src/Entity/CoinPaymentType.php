<?php

/**
 * @file
 * Contains \Drupal\cointools\Entity\CoinPaymentType.
 */

namespace Drupal\cointools\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Payment type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "coin_payment_type",
 *   label = @Translation("Payment type"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\cointools\Form\PaymentTypeForm",
 *       "edit" = "Drupal\cointools\Form\PaymentTypeForm",
 *       "delete" = "Drupal\cointools\Form\PaymentTypeDeleteConfirm"
 *     },
 *     "list_builder" = "Drupal\cointools\PaymentTypeListBuilder",
 *   },
 *   admin_permission = "administer content types",
 *   config_prefix = "type",
 *   bundle_of = "coin_payment",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/coin_payment/{coin_payment_type}",
 *     "delete-form" = "/admin/structure/coin_payment/{coin_payment_type}/delete",
 *     "collection" = "/admin/structure/coin_payment",
 *   }
 * )
 */
class CoinPaymentType extends ConfigEntityBundleBase {

  /**
   * The machine name of this payment type.
   *
   * @var string
   *
   * @todo Rename to $id.
   */
  public $type;

  /**
   * The human-readable name of the payment type.
   *
   * @var string
   *
   * @todo Rename to $label.
   */
  public $name;

  /**
   * A brief description of this payment type.
   *
   * @var string
   */
  public $description;

  /**
   * The label to use for the title of a payment of this type in the user interface.
   *
   * @var string
   *
   * @todo Rename to $payment_title_label.
   */
  public $title_label = 'Title';

  /**
   * Module-specific settings for this payment type, keyed by module name.
   *
   * @var array
   *
   * @todo Pluginify.
   */
  public $settings = [];

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleSettings($module) {
    if (isset($this->settings[$module]) && is_array($this->settings[$module])) {
      return $this->settings[$module];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked() {
    $locked = \Drupal::state()->get('node.type.locked');
    return isset($locked[$this->id()]) ? $locked[$this->id()] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
/*
    if ($this->getOriginalId() != $this->id()) {
      $update_count = coin_payment_type_update_payments($this->getOriginalId(), $this->id());
      if ($update_count) {
        drupal_set_message(format_plural($update_count,
          'Changed the content type of 1 post from %old-type to %type.',
          'Changed the content type of @count posts from %old-type to %type.',
          [
            '%old-type' => $this->getOriginalId(),
            '%type' => $this->id(),
          ]));
      }
    }*/
    if ($update) {
      // Clear the cached field definitions as some settings affect the field
      // definitions.
      $this->entityManager()->clearCachedFieldDefinitions();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Clear the payment type cache to reflect the removal.
    $storage->resetCache(array_keys($entities));
  }

}
