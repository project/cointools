<?php

/**
 * @file
 * Definition of Drupal\cointools\Form\PaymentForm.
 */

namespace Drupal\cointools\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the payment edit forms.
 */
class PaymentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $payment = $this->buildEntity($form, $form_state);
    $payment->save();
    $form_state->setRedirect('cointools.coin_payment.canonical', ['coin_payment' => $payment->id()]);
  }

}
