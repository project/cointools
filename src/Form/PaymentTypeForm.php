<?php

/**
 * @file
 * Contains \Drupal\cointools\NodeTypeForm.
 */

namespace Drupal\cointools\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\cointools\CoinTools;
use Drupal\cointools\AddressSourceManager;
use BitWasp\BitcoinLib\BIP32;

/**
 * Form controller for payment type forms.
 */
class PaymentTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = SafeMarkup::checkPlain($this->t('Add payment type'));
    }
    elseif ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label payment type', ['%label' => $type->label()]);
    }

    $form['name'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => $type->name,
      '#description' => t('The human-readable name of this payment type. This text will be displayed as part of the list on the <em>Add payment</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['type'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => $type->isLocked(),
      '#machine_name' => [
        'exists' => ['\Drupal\cointools\Entity\CoinPaymentType', 'load'],
        'source' => ['name'],
      ],
      '#description' => t('A unique machine-readable name for this payment type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %payment-add page, in which underscores will be converted into hyphens.', [
        '%payment-add' => t('Add payment'),
      ]),
    ];

    $form['description'] = [
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->description,
      '#description' => t('Describe this payment type. The text will be displayed on the <em>Add payment</em> page.'),
    ];

    $form += CoinTools::addressSourceFormItems((array) $type);

    $form['certificate'] = [
      '#title' => t('Certificate'),
      '#type' => 'textarea',
      '#default_value' => isset($type->certificate) ? $type->certificate : '',
      '#description' => t("A PEM formatted X.509 certificate."),
      '#rows' => 20,
      '#attributes' => ['class' => ['cointools-monospace']],
    ];

    $form['private_key'] = [
      '#title' => t('Private key'),
      '#type' => 'textarea',
      '#default_value' => isset($type->private_key) ? $type->private_key : '',
      '#description' => t("A PEM formatted private key."),
      '#rows' => 20,
      '#attributes' => ['class' => ['cointools-monospace']],
    ];

    $form['encrypted'] = [
      '#title' => t("Private key is encrypted"),
      '#type' => 'checkbox',
      '#default_value' => isset($type->passphrase) && $type->passphrase != '',
    ];

    $form['passphrase'] = [
      '#title' => t("Set passphrase"),
      '#type' => 'password',
      '#description' => t("The passphrase only needs to be entered if it has not yet been set, or it is to be changed."),
      '#states' => [
        'invisible' => [
          ':input[name="encrypted"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = t('Save payment type');
    $actions['delete']['#value'] = t('Delete payment type');
    return $actions;
  }


  /**
   * {@inheritdoc}
   */
  public function validate(array $form, FormStateInterface $form_state) {
    parent::validate($form, $form_state);
    $values = $form_state->getValues();

    $id = trim($values['type']);
    // '0' is invalid, since elsewhere we check it using empty().
    if ($id == '0') {
      $form_state->setErrorByName('type', $this->t("Invalid machine-readable name. Enter a name other than %invalid.", ['%invalid' => $id]));
    }

    $item_name = 'cointools_address_source_config_' . $values['cointools_address_source'];
    if (isset($values[$item_name])) {
      $plugin = \Drupal::service('cointools.address_source_manager')->createInstance($values['cointools_address_source']);
      $plugin->validate($form_state, $values[$item_name]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = &$form_state->getValues();

    if (!$values['encrypted']) {
      $values['passphrase'] = '';
    }
    elseif ($values['passphrase'] == '') {
      unset($values['passphrase']);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $type = $this->entity;
    $type->type = trim($type->id());
    $type->name = trim($type->name);

    $source_config = $form_state->getValue('cointools_address_source_config_' . $type->cointools_address_source);
    $type->cointools_address_source_config = serialize($source_config);

    $status = $type->save();

    $t_args = ['%name' => $type->label()];

    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('The payment type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message(t('The payment type %name has been added.', $t_args));
      $url = Url::fromRoute('entity.coin_payment_type.collection');
      $context = array_merge($t_args, ['link' => \Drupal::l(t('View'), $url)]);
      $this->logger('cointools')->notice('Added payment type %name.', $context);
    }

    $form_state->setRedirect('entity.coin_payment_type.collection');
  }

}
