<?php
/**
* @file
* Contains \Drupal\cointools\Form\AdminForm.
*/

namespace Drupal\cointools\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Defines a form to configure maintenance settings for this site.
*/
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'cointools_admin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cointools.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cointools.settings');

    $form['testnet'] = [
      '#type' => 'checkbox',
      '#title' => t("Testnet"),
      '#default_value' => $config->get('testnet'),
      '#description' => t("Use testnet instead of mainnet."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('cointools.settings');
    $config->set('testnet', $form_state->getValue('testnet'));
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
