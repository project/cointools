<?php

/**
 * @file
 * Definition of Drupal\cointools\CoinPaymentViewBuilder.
 */

namespace Drupal\cointools;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\cointools\Entity\CoinPayment;
use Drupal\user\Entity\User;
use Drupal\cointools_daemon\Client as DaemonClient;
use Drupal\shapeshift\ShapeShift;
use BitWasp\BitcoinLib\BitcoinLib;
use Drupal\Core\Url;

/**
 * View builder for coin payments.
 */
class CoinPaymentViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $coin_payment, EntityViewDisplayInterface $display, $view_mode, $langcode = NULL) {
    /** @var \Drupal\cointools\CoinPaymentInterface $entity */
    parent::alterBuild($build, $coin_payment, $display, $view_mode, $langcode);

    try {
      if (\Drupal::moduleHandler()->moduleExists('cointools_daemon')) {
        $client = DaemonClient::factory();
      }
      else {
        throw new \Exception("No blockchain source.");
      }
    }
    catch (\Exception $exception) {
      return $build;
    }

    $address = BitcoinLib::base58_encode(bin2hex($coin_payment->get('address')->value));
    $amount = CoinTools::bitcoinToSatoshi($client->request('getreceivedbyaddress', [$address, 0]));

    $build['amount_received'] = [
      '#type' => 'item',
      '#title' => "Total amount sent to address:",
      'amount' => [
        '#type' => 'cointools_amount',
        '#amount' => $amount,
      ],
      '#weight' => 1,
    ];

    for ($c = 6; $c > 0; $c--) {
      $confirmed_amount = CoinTools::bitcoinToSatoshi($client->request('getreceivedbyaddress', [$address, $c]));
      if ($confirmed_amount >= $coin_payment->get('amount')->value) {
        break;
      }
    }

    $build['confirmations'] = [
      '#type' => 'item',
      '#title' => "Confirmations for required amount:",
      '#markup' => $c . '<span style="white-space:nowrap;">&thinsp;</span>/<span style="white-space:nowrap;">&thinsp;</span>6',
      '#weight' => 2,
    ];

    $expired = (!is_null($coin_payment->get('expires')->value) && strtotime($coin_payment->get('expires')->value) < time());

    // Only display QR code if payment has not been received and the payment has
    // not expired.
    if (is_null($coin_payment->get('paid')->value) && !$expired) {
      // Create bitcoin URI.
      $components = [
        'address' => $address,
        'amount' => $coin_payment->get('amount')->value,
        'label' => $coin_payment->get('memo')->value,
        'r' => Url::fromRoute('cointools.coin_payment.request', ['coin_payment' => $coin_payment->get('pid')->value], ['absolute' => TRUE])->toString(),
      ];
      $bitcoin_uri = CoinTools::bitcoinUri($components);
      $build['qr'] = [
        '#type' => 'cointools_qr',
        '#uri' => $bitcoin_uri,
        '#weight' => 3,
      ];
      $build['#attached']['library'][] = 'cointools/check-payment';
      $build['#attached']['drupalSettings'] = ['cointools' => ['pid' => $coin_payment->get('pid')->value]];
      // Support http://challengepost.com/software/zero-click-bitcoin-micropayments
      $build['#attached']['http_header'][] = ['Status', 402];
      $build['#attached']['http_header'][] = ['Bitcoin-Payment-URI', $bitcoin_uri];
      $build['#attached']['http_header'][] = ['Bitcoin-Paid-URI', \Drupal::request()->getRequestUri()];

      if (\Drupal::moduleHandler()->moduleExists('shapeshift')) {
        $build['shapeshift'] = [
          '#type' => 'item',
          '#title' => t("Or"),
          '#markup' => ShapeShift::button($address, $coin_payment->get('amount')->value),
          '#weight' => 4,
        ];
      }
    }

  }

}
