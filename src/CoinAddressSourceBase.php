<?php

/**
 * @file
 * Contains \Drupal\cointools\CoinAddressSourceBase;
 */

namespace Drupal\cointools;

use Drupal\Core\Form\FormStateInterface;

abstract class CoinAddressSourceBase {

  function form() {
    return [];
  }

  function validate(FormStateInterface $form_state, array $values) {
  }

  function confirmed($txid, $address, array $config) {
  }

}
