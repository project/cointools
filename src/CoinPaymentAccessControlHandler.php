<?php

/**
 * @file
 * Contains \Drupal\cointools\CoinPaymentAccessControlHandler.
 */

namespace Drupal\cointools;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Defines the access control handler for the coin payment entity type.
 *
 * @see \Drupal\cointools\Entity\CoinPayment
 * @ingroup node_access
 */
class CoinPaymentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $node, $operation, $langcode, AccountInterface $account) {
    return AccessResult::allowed();
  }

}
