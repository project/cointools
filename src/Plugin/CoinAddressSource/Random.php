<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\CoinAddressSource\Random.
 */

namespace Drupal\cointools\Plugin\CoinAddressSource;

use Drupal\cointools\CoinTools;
use Drupal\cointools\CoinAddressSourceBase;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Provides source of random addresses.
 *
 * @Plugin(
 *   id = "cointools_random",
 *   title = @Translation("Random (burn coins)"),
 * )
 */
class Random extends CoinAddressSourceBase {

  function getAddress() {
    $hash160 = bin2hex(openssl_random_pseudo_bytes(20));
    $prefix = str_pad(dechex(CoinTools::prefixes()['p2pkh']), 2, '0');
    return BitcoinLib::hash160_to_address($hash160, $prefix);
  }
}
