<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\CoinAddressSource\Hd.
 */

namespace Drupal\cointools\Plugin\CoinAddressSource;

use Drupal\cointools\CoinTools;
use Drupal\cointools\CoinAddressSourceBase;
use Drupal\Core\Form\FormStateInterface;
use BitWasp\BitcoinLib\BIP32;

/**
 * Provides source addresses from an HD wallet.
 *
 * @Plugin(
 *   id = "cointools_hd",
 *   title = @Translation("HD wallet"),
 * )
 */
class Hd extends CoinAddressSourceBase {

  function getAddress($config) {
    return CoinTools::getHdAddress($config['xpub']);
  }

  function form() {
    $form['xpub'] = [
      '#type' => 'textfield',
      '#title' => t("Extended public key"),
      '#description' => t(""),
      '#attributes' => ['class' => ['cointools-monospace']],
    ];

    $form['interrogate'] = [
      '#value' => t("Interrogate"),
      '#type' => 'button',
      '#ajax' => [
        'callback' => ['\Drupal\cointools\Plugin\CoinAddressSource\Hd', 'interrogateXpub'],
        'wrapper' => 'xpub-info',
        'method' => 'replace',
        'effect' => 'fade',
        'progress' => ['type' => 'none'],
      ],
      '#executes_submit_callback' => false,
      '#limit_validation_errors' => [],
      '#id' => 'myuniqueid',
    ];

    $form['info'] = [
      '#prefix' => '<div id="xpub-info">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  function validate(FormStateInterface $form_state, array $values) {
    if ($values['xpub'] != '') {
      $info = BIP32::import($values['xpub']);

      $field_name = 'cointools_address_source_config_cointools_hd';
      if ($info['type'] != 'public') {
        $form_state->setErrorByName($field_name, t("Extended public key is not public."));
        $field_name .= '+';
      }

      if ($info['network'] != 'bitcoin') {
        $form_state->setErrorByName($field_name, t("Extended public key is not for Bitcoin."));
        $field_name .= '+';
      }

      if ($info['testnet'] && !CoinTools::testnet()) {
        $form_state->setErrorByName($field_name, t("Extended public key is for testnet."));
      }
    }
  }

  static function interrogateXpub(array $form, FormStateInterface $form_state) {
    $info_render = [
      '#prefix' => '<div id="xpub-info">',
      '#suffix' => '</div>',
    ];

    $xpub = $form_state->getValue(['cointools_address_source_config_cointools_hd', 'xpub']);
    if ($xpub != '') {
      $info = BIP32::import($xpub);

      $version_bytes = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['magic_bytes'],
      ];

      $depth = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['depth'],
      ];

      $fingerprint = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['fingerprint'],
      ];

      $address_number = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['address_number'],
      ];

      $chain_code = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['chain_code'],
      ];

      $key = [
        '#theme' => 'cointools_monospace',
        '#markup' => $info['key'],
      ];

      $rows = [
        [t("Version bytes"), drupal_render($version_bytes)],
        [
          'data' => [t("Type"), ucfirst($info['type'])],
          'class' => ($info['type'] == 'public') ? [] : ['error'],
        ],
        [
          'data' => [t("Network"), ($info['network'] == 'bitcoin') ? t("Any") : ucfirst($info['network'])],
          'class' => ($info['network'] == 'bitcoin') ? [] : ['error'],
        ],
        [
          'data' => [t("Testnet"), $info['testnet'] ? t("Yes") : t("No")],
          'class' => ($info['testnet'] && !CoinTools::testnet()) ? ['error'] : [],
        ],
        [t("Depth"), drupal_render($depth)],
        [t("Parent fingerprint"), drupal_render($fingerprint)],
        [t("Address number"), drupal_render($address_number)],
        [t("Chain code"), drupal_render($chain_code)],
        [t("Key"), drupal_render($key)],
      ];

      for ($i = 0; $i < 4; $i++) {
        $address = [
          '#theme' => 'cointools_monospace',
          '#markup' => CoinTools::getHdAddress($xpub, $i),
        ];
        $rows[] = [t("Address <span class=\"cointools-monospace\">0/@i</span>", ['@i' => $i]), drupal_render($address)];
      }

      $path = [
        '#theme' => 'cointools_monospace',
        '#markup' => '0/' . \Drupal::state()->get('cointools.xpubs.' . $xpub, 0),
      ];
      $rows[] = [t("Next path"), drupal_render($path)];

      $info_render += [
        '#type' => 'item',
        'info' => [
          '#theme' => 'table',
          '#rows' => $rows,
        ],
      ];
    }

    return $info_render;
  }
}
