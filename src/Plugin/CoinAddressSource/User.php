<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\CoinAddressSource\User.
 */

namespace Drupal\cointools\Plugin\CoinAddressSource;

use Drupal\cointools\CoinAddressSourceBase;

/**
 * Provides source of random addresses.
 *
 * @Plugin(
 *   id = "cointools_user",
 *   title = @Translation("Current user"),
 * )
 */
class User extends CoinAddressSourceBase {

  function getAddress() {
    $account = \Drupal::currentUser();
    $account_data = \Drupal::service('user.data')->get('cointools', $account->id());
    $source = \Drupal::service('cointools.address_source_manager')->createInstance($account_data['cointools_address_source']);
    return $source->getAddress(unserialize($account_data['cointools_address_source_config']));
  }

  function confirmed($txid, $address, array $config) {
    $account = \Drupal::currentUser();
    $account_data = \Drupal::service('user.data')->get('cointools', $account->id());
    $source = \Drupal::service('cointools.address_source_manager')->createInstance($account_data['cointools_address_source']);
    $source->confirmed($txid, $address, $account_data);
  }

}
