<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Filter\FilterBitcoinSymbol.
 */

namespace Drupal\cointools\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to convert &bitcoin; into the Bitcoin symbol.
 *
 * @Filter(
 *   id = "cointools_bitcoin_symbol",
 *   title = @Translation("Convert &bitcoin; into <span class=""bitcoin-font"">&#59392;</span>"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterBitcoinSymbol extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = str_replace(['&bitcoin;', '&amp;bitcoin;'], '<span class="bitcoin-font">&#59392;</span>', $text);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t("Convert &bitcoin; into <span class=\"bitcoin-font\">&#59392;</span>.");
  }

}
