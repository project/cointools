<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldType\AmountItem.
 */

namespace Drupal\cointools\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'cointools_amount' field type.
 *
 * @FieldType(
 *   id = "cointools_amount",
 *   label = @Translation("Coin Amount"),
 *   description = @Translation("This field stores coin amounts in the database."),
 *   default_widget = "cointools_amount",
 *   default_formatter = "cointools_amount"
 * )
 */
class AmountItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'size' => 'big',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t("Amount"));
    return $properties;
  }

}
