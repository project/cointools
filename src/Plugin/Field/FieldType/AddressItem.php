<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldType\AddressItem.
 */

namespace Drupal\cointools\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cointools\CoinTools;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Plugin implementation of the 'cointools_address' field type.
 *
 * @FieldType(
 *   id = "cointools_address",
 *   label = @Translation("Coin Address"),
 *   description = @Translation("This field stores coin addresses in the database."),
 *   default_widget = "cointools_address_textfield",
 *   default_formatter = "cointools_address_text"
 * )
 */
class AddressItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'length' => 25,
          'mysql_type' => 'BINARY(25)',
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $address = BitcoinLib::base58_encode(bin2hex($this->value));
    return $address === NULL || $address === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t("Address"));
    return $properties;
  }

}
