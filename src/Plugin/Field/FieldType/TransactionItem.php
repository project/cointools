<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldType\TransactionItem.
 */

namespace Drupal\cointools\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'cointools_transaction' field type.
 *
 * @FieldType(
 *   id = "cointools_transaction",
 *   label = @Translation("Coin Transaction"),
 *   description = @Translation("This field stores coin addresses in the database."),
 *   default_widget = "cointools_transaction_textfield",
 *   default_formatter = "cointools_transaction_text"
 * )
 */
class TransactionItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'blob',
          'length' => 32,
          'mysql_type' => 'BINARY(32)',
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t("Transaction"));
    return $properties;
  }

}
