<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldWidget\AddressWidget.
 */

namespace Drupal\cointools\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\cointools\CoinTools;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Plugin implementation of the 'cointools_address_textfield' widget.
 *
 * @FieldWidget(
 *   id = "cointools_address_textfield",
 *   label = @Translation("Text Field"),
 *   field_types = {
 *     "cointools_address"
 *   }
 * )
 */
class AddressWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => BitcoinLib::base58_encode(bin2hex($items[$delta]->value)),
      '#element_validate' => [[get_class($this), 'validateAddress']],
      '#attributes' => ['class' => ['cointools_address']],
      '#size' => 34,
    ];

    return $element;
  }

  public static function validateAddress($element, FormStateInterface $form_state) {

    if ($element['#value'] == '') {
      return;
    }

    if (!CoinTools::validateAddress($element['#value'])) {
      $message = CoinTools::testnet() ? t("Bitcoin testnet address is invalid.") : t("Bitcoin address is invalid.");
      $form_state->setError($element, $message);
    }

  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    foreach ($values as &$item) {
      if ($item['value'] !== '' && $item['value'] !== NULL) {
        $item['value'] = hex2bin(BitcoinLib::base58_decode($item['value']));
      }
    }

    return $values;
  }

}
