<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldWidget\AmountWidget.
 */

namespace Drupal\cointools\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cointools_amount' widget.
 *
 * @FieldWidget(
 *   id = "cointools_amount",
 *   label = @Translation("Text Field"),
 *   field_types = {
 *     "cointools_amount"
 *   }
 * )
 */
class AmountWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    if ($items[$delta]->value !== '' && $items[$delta]->value !== NULL) {
      list ($unit, $amount) = cointools_unit_amount($items[$delta]->value);
    }
    else {
      $unit = 'BTC';
      $amount = '';
    }

    $units = cointools_units();

    foreach ($units as $key => &$value) {
      $value = $key;
    }

    $element['unit'] = [
      '#type' => 'select',
      '#options' => $units,
      '#default_value' => $unit,
      '#weight' => 0,
      '#title' => $element['#title'],
      '#required' => $element['#required'],
    ];

    $element['value'] = [
      '#type' => 'number',
      '#default_value' => $amount,
      '#weight' => 1,
      '#step' => 'any',
      '#min' => 0,
      '#description' => $element['#description'],
    ];

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    foreach ($values as &$item) {
      if ($item['value'] !== '' && $item['value'] !== NULL) {
        $units = array_keys(cointools_units());
        $exp = array_search($item['unit'], $units);
        $item['value'] *= pow(1000, $exp) * 100;
      }
    }

    return $values;
  }

}
