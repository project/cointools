<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldWidget\TransactionWidget.
 */

namespace Drupal\cointools\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cointools_transaction_textfield' widget.
 *
 * @FieldWidget(
 *   id = "cointools_transaction_textfield",
 *   label = @Translation("Text Field"),
 *   field_types = {
 *     "cointools_transaction"
 *   }
 * )
 */
class TransactionWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => bin2hex($items[$delta]->value),
    ];

    return $element;
  }

  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

    foreach ($values as &$item) {
      if ($item['value'] !== '' && $item['value'] !== NULL) {
        $item['value'] = hex2bin($item['value']);
      }
    }

    return $values;
  }

}
