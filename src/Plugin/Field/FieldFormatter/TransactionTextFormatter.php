<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\field\formatter\TransactionTextFormatter.
 */

namespace Drupal\cointools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'cointools_transaction_text' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_transaction_text",
 *   label = @Translation("Text"),
 *   field_types = {
 *     "cointools_transaction",
 *   },
 * )
 */
class TransactionTextFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => bin2hex($item->value),
      ];
    }

    return $elements;
  }

}
