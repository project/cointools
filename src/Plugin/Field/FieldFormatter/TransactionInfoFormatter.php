<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\field\formatter\TransactionInfoFormatter.
 */

namespace Drupal\cointools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'cointools_transaction_info' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_transaction_info",
 *   label = @Translation("Info"),
 *   field_types = {
 *     "cointools_transaction",
 *   },
 * )
 */
class TransactionInfoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $transaction = [
        '#type' => 'cointools_transaction',
        '#txn_id' => bin2hex($item->value),
      ];
      $elements[$delta] = $transaction;
    }

    return $elements;
  }

}
