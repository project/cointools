<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\field\formatter\AddressQRCodeFormatter.
 */

namespace Drupal\cointools\Plugin\Field\FieldFormatter;

use Drupal\cointools\CoinTools;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Plugin implementation of the 'cointools_address_qrcode' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_address_qrcode",
 *   label = @Translation("QR Code"),
 *   field_types = {
 *     "cointools_address",
 *   }
 * )
 */
class AddressQRCodeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'width' => 200,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['width'] = [
      '#title' => t("Width"),
      '#type' => 'number',
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t("Width: @width", ['@width' => $this->getSetting('width')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];
    // Generate QR code for each item.
    foreach ($items as $delta => $item) {
      $address = BitcoinLib::base58_encode(bin2hex($item->value));
      $elements[$delta] = [
        '#type' => 'cointools_qr',
        '#uri' => CoinTools::bitcoinUri(['address' => $address]),
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('width'),
      ];
    }
    return $elements;
  }

}
