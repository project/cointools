<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\field\formatter\AddressTextFormatter.
 */

namespace Drupal\cointools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Plugin implementation of the 'cointools_address_text' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_address_text",
 *   label = @Translation("Text"),
 *   field_types = {
 *     "cointools_address",
 *   },
 * )
 */
class AddressTextFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $address = BitcoinLib::base58_encode(bin2hex($item->value));
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => '<a href="bitcoin:' . $address . '">' . $address . '</a>',
        '#prefix' => '<span class="cointools-monospace">',
        '#suffix' => '</span>',
      ];
    }

    return $elements;
  }

}
