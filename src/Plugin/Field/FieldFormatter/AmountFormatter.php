<?php

/**
 * @file
 * Contains \Drupal\cointools\Plugin\Field\FieldFormatter\AmountFormatter.
 */

namespace Drupal\cointools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cointools_amount' formatter.
 *
 * @FieldFormatter(
 *   id = "cointools_amount",
 *   label = @Translation("Amount"),
 *   field_types = {
 *     "cointools_amount",
 *   }
 * )
 */
class AmountFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
     'unit' => 'auto',
     'unit_display' => TRUE,
     'decimal_places' => NULL,
     'fiat' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options['auto'] = t("Auto");
    $units = cointools_units();
    foreach ($units as $key => $value) {
      $options[$key] = $key;
    }
    $element['unit'] = [
      '#title' => t("Unit"),
      '#type' => 'select',
      '#default_value' => $this->getSetting('unit'),
      '#options' => $options,
    ];
    $element['unit_display'] = [
      '#title' => t("Display unit"),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('unit_display'),
    ];
    $element['decimal_places'] = [
      '#title' => t("Decimal places"),
      '#type' => 'number',
      '#default_value' => $this->getSetting('decimal_places'),
      '#min' => 0,
      '#max' => 15,
    ];
    $element['fiat'] = [
      '#title' => t("Fiat"),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('fiat'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t("Unit: @unit", ['@unit' => $this->getSetting('unit')]);
    $summary[] = t("Display unit: @unit_display", ['@unit_display' => $this->getSetting('unit_display') ? t("True") : t("False")]);
    if (is_numeric($this->getSetting('decimal_places'))) {
      $summary[] = t("Decimal places: @decimal_places", ['@decimal_places' => $this->getSetting('decimal_places')]);
    }
    $summary[] = t("Fiat: @fiat", ['@fiat' => $this->getSetting('fiat') ? t("True") : t("False")]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = [];
    $units = cointools_units();
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'cointools_amount',
        '#amount' => $item->value,
        '#unit' => $this->getSetting('unit'),
        '#unit_display' => $this->getSetting('unit_display'),
        '#decimal_places' => $this->getSetting('decimal_places'),
        '#fiat' => $this->getSetting('fiat'),
      ];
    }

    return $elements;
  }

}
