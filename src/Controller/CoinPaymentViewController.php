<?php

/**
 * @file
 * Contains \Drupal\cointools\Controller\CoinPaymentViewController.
 */

namespace Drupal\cointools\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Url;
use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;
use Drupal\shapeshift\ShapeShift;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Defines a controller to render a single coin payment.
 */
class CoinPaymentViewController extends EntityViewController {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $coin_payment, $view_mode = 'full', $langcode = NULL) {
    $build = ['payment' => parent::view($coin_payment)];

    return $build;
  }

  /**
   * The _title_callback for the cointools.coin_payment.canonical route.
   *
   * @param \Drupal\Core\Entity\EntityInterface $coin_payment
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $coin_payment) {
    return t("Bitcoin payment @id", array('@id' => $coin_payment->id()));
  }

}
