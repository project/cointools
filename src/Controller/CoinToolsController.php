<?php

/**
 * @file
 * Contains \Drupal\cointools\Controller\CoinToolsController.
 */

namespace Drupal\cointools\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use payments\Output;
use payments\PaymentDetails;
use payments\X509Certificates;
use payments\PaymentRequest;
use payments\Payment;
use payments\PaymentACK;
use Drupal\cointools\CoinTools;
use Drupal\cointools_daemon\Client as DaemonClient;
use BitWasp\BitcoinLib\BitcoinLib;

/**
 * Controller routines for Coin Daemon routes.
 */
class CoinToolsController extends ControllerBase {

  /**
   * Displays add payment links for available payment types.
   *
   * Redirects to admin/bitcoin/add-payment/[type] if only one payment type is
   * available.
   *
   * @return array
   *   A render array for a list of the payment types that can be added;
   *   however, if there is only one payment type defined for the site, the
   *   function redirects to the payment add page for that one node type and
   *   does not return at all.
   */
  public function addPage() {
    $content = [];

    foreach ($this->entityManager()->getStorage('coin_payment_type')->loadMultiple() as $type) {
      $content[$type->id()] = $type;
    }

    // Bypass the node/add listing if only one content type is available.
    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('cointools.payment_add', ['coin_payment_type' => $type->id()]);
    }

    return [
      '#theme' => 'cointools_payment_add_list',
      '#content' => $content,
    ];
  }

  /**
   * Provides the payment submission form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $coin_payment_type
   *   The payment type entity for the payment.
   *
   * @return array
   *   A payment submission form.
   */
  public function add(EntityInterface $coin_payment_type) {

    $payment = $this->entityManager()->getStorage('coin_payment')->create([
      'type' => $coin_payment_type->type,
    ]);

    $form = $this->entityFormBuilder()->getForm($payment);

    return $form;
  }

  public function paymentRequest(EntityInterface $coin_payment) {
    header('Content-Type: application/bitcoin-paymentrequest');
    header('Content-Transfer-Encoding: binary');

    require_once 'core/vendor/bluedroplet/bitcoin-payment-protoc-php/paymentrequest.php';

    // Check if the payment has already been fulfilled or expired.
    if (!is_null($coin_payment->get('paid')->value) || (!is_null($coin_payment->get('expires')->value) && strtotime($coin_payment->get('expires')->value) < time())) {
      header($_SERVER['SERVER_PROTOCOL'] . ' 410 Gone');
      exit;
    }

    $address = BitcoinLib::base58_encode(bin2hex($coin_payment->get('address')->value));

    $output = new Output();
    $output->setAmount($coin_payment->get('amount')->value);
    // Create a P2PKH script: https://bitcoin.org/en/developer-guide#p2pkh-script-validation
    $pubkeyhash = BitcoinLib::base58_decode_checksum($address);
    $output->setScript(hex2bin('76a914' . $pubkeyhash . '88ac'));

    $payment_details = new PaymentDetails();
    $payment_details->setNetwork(CoinTools::testnet() ? 'test' : 'main');
    $payment_details->setOutputs([$output]);
    $payment_details->setTime(strtotime($coin_payment->get('time')->value));

    if (!is_null($coin_payment->get('expires')->value)) {
      $payment_details->setExpires(strtotime($coin_payment->get('expires')->value));
    }

    if ($coin_payment->get('memo')->value != '') {
      $payment_details->setMemo($coin_payment->get('memo')->value);
    }

    $url = Url::fromRoute('cointools.coin_payment.payment', ['coin_payment' => $coin_payment->pid->value], ['absolute' => TRUE])->toString();
    $payment_details->setPaymentUrl($url);

    $payment_request = new PaymentRequest();
    $payment_request->setSerializedPaymentDetails($payment_details->serialize());

    $bundle_name = $coin_payment->bundle();
    $bundle = entity_load('coin_payment_type', $bundle_name);

    if ($bundle->certificate != '') {
      $payment_request->setPkiType('x509+sha256');

      $certificates = new X509Certificates();
      $certs = $this->fetchChain($bundle->certificate);
      foreach ($certs as $cert) {
        $certificates->addCertificate($cert);
      }
      $payment_request->setPkiData($certificates->serialize());
      // Before serialization, the signature field must be set to an empty value
      // so that the field is included in the signed PaymentRequest hash but
      // contains no data.
      $payment_request->setSignature('');

      $signature = '';
      $private_key = openssl_pkey_get_private($bundle->private_key, $bundle->passphrase);

      if (!openssl_sign($payment_request->serialize(), $signature, $private_key, OPENSSL_ALGO_SHA256)) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
        exit;
      }
      $payment_request->setSignature($signature);
    }

    echo $payment_request->serialize();
    exit;
  }

  public function payment(EntityInterface $coin_payment) {
    header('Content-Type: application/bitcoin-paymentack');
    header('Content-Transfer-Encoding: binary');

    require_once 'core/vendor/bluedroplet/bitcoin-payment-protoc-php/paymentrequest.php';

    // Check if the payment has expired.
    if (!is_null($coin_payment->get('expires')->value) && strtotime($coin_payment->get('expires')->value) < time()) {
      header($_SERVER['SERVER_PROTOCOL'] . ' 410 Gone');
      exit;
    }

    $data = file_get_contents("php://input");
    if (strlen($data) > 50000) {
      header($_SERVER['SERVER_PROTOCOL'] . ' 413 Request Entity Too Large');
      exit;
    }
    $payment = new Payment();
    $payment->parse($data);

    // Check if the payment has already been paid with the payment protocol.
    if ($coin_payment->get('transactions')->isEmpty()) {
      $transactions = $payment->getTransactionsList();

      $client = DaemonClient::factory();
      $address = BitcoinLib::base58_encode(bin2hex($coin_payment->get('address')->value));

      $value = 0;
      foreach ($transactions as $raw) {
        $rawtransaction = $client->request('decoderawtransaction', [bin2hex($raw)]);

        foreach ($rawtransaction['vout'] as $out) {
          if ($out['scriptPubKey']['addresses'][0] == $address) {
            $value += CoinTools::bitcoinToSatoshi($out['value']);
          }
        }
      }

      if ($value < $coin_payment->get('amount')->value) {
        $paymentACK = new PaymentACK();
        $paymentACK->setPayment($payment);
        $paymentACK->setMemo('Insufficient bitcoins!');

        echo $paymentACK->serialize();
        exit;
      }

      // Broadcast the transactions.
      $i = 0;
      $transactions_field = $coin_payment->get('transactions');
      foreach ($transactions as $raw) {
        $txid = $client->request('sendrawtransaction', [bin2hex($raw)]);
        $transactions_field->set($i++, hex2bin($txid));
      }

      if ($payment->hasRefundTo()) {
        $script = $payment->getRefundTo(0)->getScript();
        // Is this script a standard Bitcoin payment?
        // https://en.bitcoin.it/wiki/Script#Standard_Transaction_to_Bitcoin_address_.28pay-to-pubkey-hash.29
        if ((substr($script, 0, 3) == "\x76\xa9\x14") && (substr($script, 23, 2) == "\x88\xac")) {
          // Determine the address from the script.
          $address = BitcoinLib::hash160_to_address(bin2hex(substr($script, 3, 20)));
          $coin_payment->set('refund_to', hex2bin(BitcoinLib::base58_decode($address)));
        }
      }

      if ($payment->hasMemo()) {
        $coin_payment->set('customer_memo', $payment->getMemo());
      }

      $coin_payment->set('paid', gmdate(DATETIME_DATETIME_STORAGE_FORMAT));
      $coin_payment->save();
    }

    $paymentACK = new PaymentACK();
    $paymentACK->setPayment($payment);
    $paymentACK->setMemo('Payment received');

    echo $paymentACK->serialize();
    exit;
  }

  public function paymentCheck(EntityInterface $coin_payment) {
    if (is_null($coin_payment->get('paid')->value)) {
      header($_SERVER['SERVER_PROTOCOL'] . ' 402 Payment Required');
    }
    else {
      header($_SERVER['SERVER_PROTOCOL'] . ' 200 OK');
    }
    exit;
  }

  /**
   * Either parse a PEM certificate, or convert DER to PEM and then parse.
   */
  static function parseCertificate($certificate) {
    $prefix = "-----BEGIN CERTIFICATE-----";
    $suffix = "-----END CERTIFICATE-----";

    if (strpos($certificate, $prefix) !== FALSE) {
      return openssl_x509_parse($certificate);
    }
    $data = $prefix . "\n";
    $data .= chunk_split(base64_encode($certificate));
    $data .= $suffix . "\n";
    return openssl_x509_parse($data);
  }

  /**
   * Given a parsed leaf certificate, fetch its parent and return its (unparsed)
   * data.
   */
  static function fetchCertificateParent($certificate) {
    $pattern = '/CA Issuers - URI:(\\S*)/';
    $matches = [];
    $count = preg_match_all($pattern, $certificate['extensions']['authorityInfoAccess'], $matches);
    if ($count == 0) {
      return FALSE;
    }
    foreach ($matches[1] as $url) {
      $parent = file_get_contents($url);
      if ($parent && self::parseCertificate($parent)) {
        return $parent;
      }
    }
    return FALSE;
  }

  static function pem2der($pem_data) {
    $prefix = "CERTIFICATE-----";
    $suffix = "-----END";
    if (strpos($pem_data, $prefix) === FALSE) {
      return $pem_data;
    }
    $pem_data = substr($pem_data, strpos($pem_data, $prefix) + strlen($prefix));
    $pem_data = substr($pem_data, 0, strpos($pem_data, $suffix));
    $der = base64_decode($pem_data);
    return $der;
  }

  /**
   * Fetch a whole certificate chain, starting with a leaf certificate. Returns
   * raw certificate data in an array, leaf certificate first.
   */
  static function fetchChain($leaf) {
    $result = [];

    $cert = self::parseCertificate($leaf);
    if ($cert === FALSE) {
      return FALSE;
    }
    $cert_data = self::pem2der($leaf);

    while ($cert !== FALSE && $cert['issuer'] != $cert['subject']) {
      $result[] = $cert_data;
      $cert_data = self::fetchCertificateParent($cert);
      $cert = self::parseCertificate($cert_data);
    }

    return $result;
  }

  /**
   * The _title_callback for the cointools.payment_add route.
   *
   * @param \Drupal\Core\Entity\EntityInterface $coin_payment_type
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle(EntityInterface $coin_payment_type) {
    return $this->t('Add %name payment', array('%name' => $coin_payment_type->label()));
  }

}
